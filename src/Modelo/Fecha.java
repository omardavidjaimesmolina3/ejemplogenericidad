/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelo;

/**
 *
 * @author docenteauditorio
 */
class Fecha implements Comparable {

    private byte dia, mes, min, seg;
    private int agno;

    public Fecha() {
    }

    public Fecha(byte dia, byte mes, int agno, byte min, byte seg) {
        this.dia = dia;
        this.mes = mes;
        this.agno = agno;
        this.min = min;
        this.seg = seg;
    }

    public byte getDia() {
        return dia;
    }

    public void setDia(byte dia) {
        this.dia = dia;
    }

    public byte getMes() {
        return mes;
    }

    public void setMes(byte mes) {
        this.mes = mes;
    }

    public int getAgno() {
        return agno;
    }

    public void setAgno(int agno) {
        this.agno = agno;
    }

    public byte getMin() {
        return min;
    }

    public void setMin(byte min) {
        this.min = min;
    }

    public byte getSeg() {
        return seg;
    }

    public void setSeg(byte seg) {
        this.seg = seg;
    }

    @Override
    public int compareTo(Object o) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public String toString() {
        return "Fecha{" + "dia=" + dia + ", mes=" + mes + ", agno=" + agno + ", min=" + min + ", seg=" + seg + '}';
    }

}
