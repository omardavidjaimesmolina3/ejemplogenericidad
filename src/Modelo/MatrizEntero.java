/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelo;

import java.util.Random;

/**
 * Clase que representa una Matriz de enteros
 *
 * @author madarme
 */
public class MatrizEntero implements Comparable{

    private int M[][];

    /**
     * Constructor vacío
     */
    public MatrizEntero() {
    }

    /**
     * Constructor para matrices cuadradas o rectangulares
     *
     * @param n un entero que representa la cantidad de filas
     * @param m un entero que representa la cantidad de columnas
     */
    public MatrizEntero(int n, int m) {

        this.validar(n);
        this.validar(m);
        this.M = new int[n][m]; //celdas vacías
        crearMatriz();
    }

    /**
     * Crea una matriz DISPERSA
     *
     * @param n cantidad de filas de lam matriz
     */
    public MatrizEntero(int n) {
        this.validar(n);
        //Crear las filas:
        this.M=new int[n][];
        //crear las columnaFs:
        for(int i=0;i<this.M.length;i++)
        {
            
            int cantCol=new Random().nextInt(7)+1;
            this.M[i]=new int[cantCol];
        }
        this.crearMatriz();
    }

    private void validar(int n) {
        if (n <= 0) {
            throw new RuntimeException("No es posible crear matriz");
        }
    }

    private void crearMatriz() {
        if (this.M == null) {
            throw new RuntimeException("Matriz sin memoria");
        }
        for (int i = 0; i < this.M.length; i++) {
            crearElementos(this.M[i]);
        }
    }

    private void crearElementos(int[] vectorFilasxCol) {
        for (int j = 0; j < vectorFilasxCol.length; j++) {
            vectorFilasxCol[j] = new Random().nextInt(200);
        }
    }

    /**
     * Obtiene la cantidad de filas
     *
     * @return un entero con la cantidad de filas
     */
    public int getNumeroFilas() {
        return this.M.length;
    }

    /**
     * Obtiene dada la posición de la fila , su cantidad de columnas
     *
     * @param i índice de la fila
     * @return un entero con la cantidad de columnas
     */
    public int getNumeroColumnas(int i) {
        if (i <= 0 || i >= M.length) {
            throw new RuntimeException("Fuera de rango");
        }
        return this.M[i].length;
    }

    @Override
    public String toString() {
        if (this.M == null) {
            return "Matriz vacía";
        }
        String msg = "";
        for (int vector[] : this.M) {
            for (int elemento : vector) {
                msg += elemento + "\t";
            }
            msg += "\n";
        }
        return msg;
    }
  
    /**
     * retorna verdadero si todos los datos de la matriz son pares o false en caso contrario
     * @return true si todos son verdaderos.
     */
    public boolean isValidopar()
    {
        return false;
    }
    
    public boolean isDispersa()
    {
    return false;
    }
    
    
    public boolean isCuadrada()
    {
    return false;
    }
    
    public boolean isRectangular()
    {
    return false;
    }
    
     /**
      *  Obtiene el menor elemento de una fila
      * @param i el indice de la fila
      * @return un entero que representa el menor de la fila
      */
    public int getMenorFila(int i)
    {
        return 0;
    }
    
    
    /**
      *  Obtiene el menor elemento de una columna
      * @param i el indice de la columna
      * @return un entero que representa el menor de la fila
      */
    public int getMenorCol(int i)
    {
        return 0;
    }

    @Override
    public int compareTo(Object o) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}
