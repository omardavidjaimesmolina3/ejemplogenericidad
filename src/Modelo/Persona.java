/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelo;

/**
 *
 * @author docenteauditorio
 */
public class Persona implements Comparable {

    private long cedula;
    private Fecha fechaNacimiento;

    public Persona() {
    }

    public Persona(long cedula, int agno, byte dia, byte mes, byte min, byte seg) {
        this.cedula = cedula;
        //Crear aquí :)
    }

    public long getCedula() {
        return cedula;
    }

    public void setCedula(long cedula) {
        this.cedula = cedula;
    }

    public Fecha getFechaNacimiento() {
        return fechaNacimiento;
    }

    public void setFechaNacimiento(Fecha fechaNacimiento) {
        this.fechaNacimiento = fechaNacimiento;
    }

    @Override
    public int compareTo(Object o) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public String toString() {
        return "Persona{" + "cedula=" + cedula + ", fechaNacimiento=" + fechaNacimiento + '}';
    }

}
