/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelo;

/**
 *
 * @author docenteauditorio
 */
public class Fraccion implements Comparable {

    private int numerador, denominador;

    public Fraccion() {
    }

    public Fraccion(int numerador, int denominador) {
        this.numerador = numerador;
        this.denominador = denominador;
    }

    public int getNumerador() {
        return numerador;
    }

    public void setNumerador(int numerador) {
        this.numerador = numerador;
    }

    public int getDenominador() {
        return denominador;
    }

    public void setDenominador(int denominador) {
        this.denominador = denominador;
    }

    @Override
    public String toString() {
        return this.numerador + "/" + this.denominador;
    }

    @Override
    public int compareTo(Object o) {
        if (!this.getClass().equals(o.getClass())) {
            throw new RuntimeException("No puedo comparar por que son de diferente tipo");
        }

        //Después de verificar el objeto o, paso a convertirlo a su base correspondiente
        Fraccion dos = (Fraccion) o;
        return (int) (this.getValorDecimal() - dos.getValorDecimal());
    }

    public float getValorDecimal() {
        if (this.denominador == 0) {
            throw new RuntimeException("Error división por cero");
        }
        return this.numerador / this.denominador;
    }
}
